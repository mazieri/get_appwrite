#!/usr/bin/bash
#
#           Script para instalar o Appwrite v0.0.1#
#   GitHub Appwrite: https://github.com/appwrite/appwrite
#   Issues Appwrite: https://github.com/appwrite/appwrite/issues
#   ---
#   GitLab Script: https://gitlab.com/mazieri/get_appwrite
#   Script: https://gitlab.com/mazieri/get_appwrite/-/raw/main/appwrite_installer_br.sh
#   Precisa: sudo/root, bash, rm, curl/wget
#
#   Este script instala o Appwrite em seu sistema.
#   Usar:
#
#   	$ wget -qO- https://gitlab.com/mazieri/get_appwrite/-/raw/main/appwrite_installer_br.sh | sudo bash
#   	  ou
#   	$ curl -fsSL https://gitlab.com/mazieri/get_appwrite/-/raw/main/appwrite_installer_br.sh | sudo bash
#
#   Se usar curl, recomendo usar as flags -fsSL.
#
#   Só funcionar/foi testado em sistemas linux. Porfavor
#   abra uma issue se achar um bug.
#
clear
echo -e "\e[0m\c"

logo() {
    echo '
        ╔═══╗                    ╔╗
        ║╔═╗║                   ╔╝╚╗
        ║║ ║║╔══╗╔══╗╔╗╔╗╔╗╔═╗╔╗╚╗╔╝╔══╗
        ║╚═╝║║╔╗║║╔╗║║╚╝╚╝║║╔╝╠╣ ║║ ║║═╣
        ║╔═╗║║╚╝║║╚╝║╚╗╔╗╔╝║║ ║║ ║╚╗║║═╣
        ╚╝ ╚╝║╔═╝║╔═╝ ╚╝╚╝ ╚╝ ╚╝ ╚═╝╚══╝
             ║║  ║║
             ╚╝  ╚╝
    '
}

att() {
    clear
    logo
    echo ""
    echo "Iniciando atualização do sistema 1 !"
    echo ""
    sleep 5
    sudo yum -y update && sudo yum -y upgrade
    echo ""
    echo "Completou atualização do sistema 1 !"
    echo ""
    sleep 5
}

repoDocker() {
    clear
    logo
    echo ""
    echo "✔ Atualização do Sistema 1"
    echo ""
    echo "Adicionando o Docker ao sistema !"
    echo ""
    sleep 5
    sudo yum install -y yum-utils
    sudo yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo
    echo ""
    echo "Docker foi adicionado ao sistema !"
    echo ""
    sleep 5
}

att2() {
    clear
    logo
    echo ""
    echo "✔ Atualização do Sistema 1"
    echo "✔ Adicionando o Docker ao sistema"
    echo ""
    echo "Iniciando atualização do sistema 2 !"
    echo ""
    sleep 5
    sudo yum -y update && sudo yum -y upgrade
    echo ""
    echo "Completou atualização do sistema 2 !"
    echo ""
    sleep 5
}

docker_ce() {
    clear
    logo
    echo ""
    echo "✔ Atualização do Sistema 1"
    echo "✔ Adicionando o Docker ao sistema"
    echo "✔ Atualização do Sistema 2"
    echo ""
    echo "Instalando o Docker !"
    echo ""
    sleep 5
    sudo yum -y install docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin
    echo ""
    echo "Docker foi Instalado !"
    echo ""
    sleep 5
}

system() {
    clear
    logo
    echo ""
    echo "✔ Atualização do Sistema 1"
    echo "✔ Adicionando o Docker ao sistema"
    echo "✔ Atualização do Sistema 2"
    echo "✔ Instalando o Docker"
    echo ""
    echo "Ativando o Docker !"
    echo ""
    sudo systemctl start docker && sudo systemctl enable docker.service && sudo systemctl enable containerd.service
    echo ""
    echo "Docker ativado !"
    echo ""
    sleep 5
}

appwrite() {
    clear
    logo
    echo ""
    echo "✔ Atualização do Sistema 1"
    echo "✔ Adicionando o Docker ao sistema"
    echo "✔ Atualização do Sistema 2"
    echo "✔ Instalando o Docker"
    echo "✔ Ativando o Docker"
    echo ""
    echo "Iniciando instalação do Appwrite !"
    echo ""
    sleep 5
    sudo docker run -i --rm \
    --volume /var/run/docker.sock:/var/run/docker.sock \
    --volume "$(pwd)"/appwrite:/usr/src/code/appwrite:rw \
    --entrypoint="install" \
    appwrite/appwrite:1.3.7
    echo ""
    echo "Appwrite instalado !"
    echo ""
    sleep 5
}

complete() {
    clear
    logo
    echo ""
    echo "✔ Atualização do Sistema 1"
    echo "✔ Adicionando o Docker ao sistema"
    echo "✔ Atualização do Sistema 2"
    echo "✔ Instalando o Docker"
    echo "✔ Ativando o Docker"
    echo "✔ Instalação do Appwrite"
    echo ""
    sleep 5
    public_ip="$(dig +short myip.opendns.com @resolver1.opendns.com)"
    echo "Terminou !"
    echo ""
    echo "Agora, se você já configurou todas as portas, pode acessar o site em: https://${public_ip}/"
    echo ""
    echo "Se você não conseguiu abrir o site, por favor, volte para a etapa de \"Configuração das Portas de Rede"\"
    echo ""
}

att && repoDocker && att2 && docker_ce && system && appwrite && complete
