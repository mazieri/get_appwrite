# MENU

- [BR](#br)
- [EN](#en)

---

## [BR](#menu)

### [Comandos](#menu)

`curl -fsSL https://gitlab.com/mazieri/get_appwrite/-/raw/main/appwrite_installer_br.sh | sudo bash`

&nbsp;

ou

&nbsp;

`wget -qO- https://gitlab.com/mazieri/get_appwrite/-/raw/main/appwrite_installer_br.sh | sudo bash`

### [Script para Instalar o Appwrite](#menu)

Script criado para facilitar a instalação do Appwrite em distros que utilizem o `yum` (padrão RedHat)

Fiquei a vontade para contribuir, seja com atualizações ou correções, é só abrir uma PR que logo irei verificar e provavelmente aprovar

---

## [EN](#menu)

### [Command](#menu)

`curl -fsSL https://gitlab.com/mazieri/get_appwrite/-/raw/main/appwrite_installer_en.sh | sudo bash`

&nbsp;

or

&nbsp;

`wget -qO- https://gitlab.com/mazieri/get_appwrite/-/raw/main/appwrite_installer_en.sh | sudo bash`

### [Script to Install Appwrite](#menu)

Script created to make easy the installation of Appwrite in distros that use `yum` (RedHat standard)

Feel free to contribute, either with updates or corrections, just open a PR and I will go check and probably approve soon
