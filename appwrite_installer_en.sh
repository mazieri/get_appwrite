#!/usr/bin/bash
#
#           Appwrite Installer Script v0.0.1#
#   GitHub Appwrite: https://github.com/appwrite/appwrite
#   Issues Appwrite: https://github.com/appwrite/appwrite/issues
#   ---
#   GitLab Script: https://gitlab.com/mazieri/get_appwrite
#   Script: https://gitlab.com/mazieri/get_appwrite/-/raw/main/appwrite_installer_en.sh
#   Requires: sudo/root, bash, rm, curl/wget
#
#   This script installs Appwrite to your system.
#   Usage:
#
#   	$ wget -qO- https://gitlab.com/mazieri/get_appwrite/-/raw/main/appwrite_installer_en.sh | sudo bash
#   	  or
#   	$ curl -fsSL https://gitlab.com/mazieri/get_appwrite/-/raw/main/appwrite_installer_en.sh | sudo bash
#
#   If using curl, we recommend using the -fsSL flags.
#
#   This only work on  Linux systems. Please
#   open an issue if you notice any bugs.
#
clear
echo -e "\e[0m\c"

logo() {
    echo '
        ╔═══╗                    ╔╗
        ║╔═╗║                   ╔╝╚╗
        ║║ ║║╔══╗╔══╗╔╗╔╗╔╗╔═╗╔╗╚╗╔╝╔══╗
        ║╚═╝║║╔╗║║╔╗║║╚╝╚╝║║╔╝╠╣ ║║ ║║═╣
        ║╔═╗║║╚╝║║╚╝║╚╗╔╗╔╝║║ ║║ ║╚╗║║═╣
        ╚╝ ╚╝║╔═╝║╔═╝ ╚╝╚╝ ╚╝ ╚╝ ╚═╝╚══╝
             ║║  ║║
             ╚╝  ╚╝
    '
}

att() {
    clear
    logo
    echo ""
    echo "Start OS update 1 !"
    echo ""
    sleep 5
    sudo yum -y update && sudo yum -y upgrade
    echo ""
    echo "Complete OS update 1 !"
    echo ""
    sleep 5
}

repoDocker() {
    clear
    logo
    echo ""
    echo "✔ Start OS update 1"
    echo ""
    echo "Start add Docker to OS !"
    echo ""
    sleep 5
    sudo yum install -y yum-utils
    sudo yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo
    echo ""
    echo "Complete add Docker to OS !"
    echo ""
    sleep 5
}

att2() {
    clear
    logo
    echo ""
    echo "✔ Start OS update 1"
    echo "✔ Add Docker to OS"
    echo ""
    echo "Start OS update 2 !"
    echo ""
    sleep 5
    sudo yum -y update && sudo yum -y upgrade
    echo ""
    echo "Complete OS update 2 !"
    echo ""
    sleep 5
}

docker_ce() {
    clear
    logo
    echo ""
    echo "✔ Start OS update 1"
    echo "✔ Add Docker to OS"
    echo "✔ Start OS update 2"
    echo ""
    echo "Start Docker Install !"
    echo ""
    sleep 5
    sudo yum -y install docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin
    echo ""
    echo "Complete Docker Install !"
    echo ""
    sleep 5
}

system() {
    clear
    logo
    echo ""
    echo "✔ Start OS update 1"
    echo "✔ Add Docker to OS"
    echo "✔ Start OS update 2"
    echo "✔ Docker Install"
    echo ""
    echo "Enable Docker !"
    echo ""
    sleep 5
    sudo systemctl start docker && sudo systemctl enable docker.service && sudo systemctl enable containerd.service
    echo ""
    echo "Docker Enabled !"
    echo ""
    sleep 5
}

appwrite() {
    clear
    logo
    echo ""
    echo "✔ Start OS update 1"
    echo "✔ Add Docker to OS"
    echo "✔ Start OS update 2"
    echo "✔ Docker Install"
    echo "✔ Enable Docker"
    echo ""
    echo "Start Appwrite Install !"
    echo ""
    sleep 5
    sudo docker run -i --rm \
    --volume /var/run/docker.sock:/var/run/docker.sock \
    --volume "$(pwd)"/appwrite:/usr/src/code/appwrite:rw \
    --entrypoint="install" \
    appwrite/appwrite:1.3.7
    echo ""
    echo "Complete Appwrite Install !"
    echo ""
    sleep 5
}

complete() {
    clear
    logo
    echo ""
    echo "✔ Start OS update 1"
    echo "✔ Add Docker to OS"
    echo "✔ Start OS update 2"
    echo "✔ Docker Install"
    echo "✔ Enable Docker"
    echo "✔ Appwrite Install"
    echo ""
    sleep 5
    public_ip="$(dig +short myip.opendns.com @resolver1.opendns.com)"
    echo "Finished !"
    echo ""
    echo "Now, if you already configured all ports, you can access the Website in: https://${public_ip}/"
    echo ""
    echo "If you couldn't open the website, please, return to step \"Network ports configuration"\"
    echo ""
}

att && repoDocker && att2 && docker_ce && system && appwrite && complete
